import requests
from os.path import isfile, join
from os import listdir

class celonisapi:

	def get_api(self, path):
		return "https://{}.{}.celonis.cloud/{}".format(self.tenant, self.realm,
													   path)

	def __init__(self, tenant, realm, api_key):
		self.tenant = tenant
		self.realm = realm
		self.api_key = api_key

	def get_jobs_api(self, pool_id):
		return self.get_api("integration/api/v1/data-push/{}/jobs/"
							.format(pool_id))

	def get_auth(self):
		return {'authorization': "Bearer {}".format(self.api_key)}

	def list_jobs(self, pool_id):
		api = self.get_jobs_api(pool_id)
		return requests.get(api, headers=self.get_auth()).json()

	def delete_job(self, pool_id, job_id):
		api = self.get_jobs_api(pool_id) + "/{}".format(job_id)
		return requests.delete(api, headers=self.get_auth())

	def create_job(self, pool_id, targetName, data_connection_id,
				   upsert=False):
		api = self.get_jobs_api(pool_id)
		job_type = "REPLACE"
		if upsert:
			job_type = "DELTA"
		if not data_connection_id:
			payload = {'targetName': targetName, 'type': job_type,
					   'dataPoolId': pool_id}
		else:
			payload = {'targetName': targetName, 'type': job_type,
					   'dataPoolId': pool_id,
					   'connectionId': data_connection_id}
		return requests.post(api, headers=self.get_auth(), json=payload).json()

	def push_new_dir(self, pool_id, job_id, dir_path,print=print):
		files = [join(dir_path, f) for f in listdir(dir_path)
				 if isfile(join(dir_path, f))]
		parquet_files = list(filter(lambda f: f.endswith(".parquet"), files))
		for parquet_file in parquet_files:
			print("Uploading chunk {}".format(parquet_file))
			self.push_new_chunk(pool_id, job_id, parquet_file)

	def push_new_chunk(self, pool_id, job_id, file_path):
		api = self.get_jobs_api(pool_id) + "/{}/chunks/upserted".format(job_id)
		upload_file = {"file": open(file_path, "rb")}
		return requests.post(api, files=upload_file, headers=self.get_auth())

	def submit_job(self, pool_id, job_id):
		api = self.get_jobs_api(pool_id) + "/{}/".format(job_id)
		return requests.post(api, headers=self.get_auth())