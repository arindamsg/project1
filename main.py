from pmdiextractor import PMDIExtractor
import sys

options = ['full','delta']

land = 'WE'
table = 'BKPF'
env = 'qa'
option = 'full'

#parse arguments
for arg in sys.argv:
    if '=' in arg:
        k,v = arg.split('=',1)
        if k == '-land':
            land = str(v)
        elif k == '-table':
            table = str(v)
        elif k == '-env':
            env = str(v)
        elif k == '-option':
            option = v
            
try:
    ext = PMDIExtractor(land, table, env)
    if not(land):
        ext.write_postgrelog("-land is rquired argument",env,option)
        #print('-land is rquired argument')
        exit()
    if not(table):
        ext.write_postgrelog("-table is rquired argument",env,option)
        #print('-table is rquired argument')
        exit()
    if not(option) or option not in options:
        ext.write_postgrelog('-option is required with one of value: {}'.format(options),env,option)
        #print('-option is required with one of value: {}'.format(options))
        exit()
    if not(env):
        env = 'mvp'

#run extractor
    #if option == options[0]:
    ext.write_postgrelog("Started parquet creation.",env,option)
    ext.extract_full()
    ext.write_postgrelog("Completed parquet creation.",env,option)
    if option == "full":
        ext.retention_full()
    elif option == "delta":
        ext.retention_inc()
    ext.write_postgrelog("Started parquet export to celonis creation.",env,option)
    #ext.export_full()
    ext.write_postgrelog("Completed parquet export to celonis creation.",env,option)

    #elif option ==options[1]:
        #ext.extract_last_job()
        #ext.export_last_job()
except Exception as e:
    ext.write_postgrelog("Exception occured." + str(e) ,env,option)