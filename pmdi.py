import os
import importlib

class PMDI:
	
	sap_tables = []

	sap_lands = []

	sql_tables = {}

	columns = {}

	data_filters = {}

	table_atributes = {}

	celonis_config = {}

	jobs = {}

	postgresql_host = "127.0.0.1"
	postgresql_port = "5432"
	postgrseql_db = "test"
	postgresql_user = "postgres"
	postgresql_pwd = "admin"
	retention = "'30'"


	def __init__(self, env):
		env = str(env).lower()
		path = './config/{}.py'.format(env)
		if os.path.isfile(path):
			module = 'config.{}'.format(env)
			config = importlib.import_module(module)
			self.postgresql_host = config.postgresql_host
			self.postgresql_port = config.postgresql_port
			self.postgresql_db = config.postgresql_db
			self.postgresql_user = config.postgresql_user
			self.postgresql_pwd = config.postgresql_pwd
			self.retention = config.retention
			try: 
				self.sap_lands = config.sap_lands
			except: 
				print('no sap_lands found in config')
			try: 
				self.sap_tables = config.sap_tables
			except: 
				print('no sap_tables found in config')
			try: 
				self.sql_tables = config.sql_tables
			except: 
				print('no sql_tables found in config')
			try: 
				self.columns = config.columns
			except: 
				print('no columns found in config')
			try: 
				self.data_filters = config.data_filters
			except: 
				print('no data_filters found in config')
			try: 
				self.table_atributes = config.table_atributes
			except: 
				print('no table_atributes found in config')
			try: 
				self.celonis_config = config.celonis_config
			except: 
				print('no celonis_config found in config')
			try: 
				self.jobs = config.jobs
			except: 
				print('no jobs found in config')
	
	def get_job_name(self, jtype='delta',land='WE', table=''):
		jobs_dict = self.jobs.get(jtype)
		if jobs_dict and type(jobs_dict) == dict:
			if jobs_dict.get(land):
				if jtype == 'delta':
					if type(jobs_dict.get(land)) == str:
						return jobs_dict.get(land)
				else:
					land_jobs = jobs_dict.get(land)
					if type(land_jobs) == dict:
						if table in land_jobs and type(land_jobs.get(table)) == 'str':
							return land_jobs.get(table)
		return

	def get_table_atribute(self, table, atribute=''):
		table = str(table).upper()
		atribute = str(atribute).lower()
		if table in self.table_atributes:
			if atribute == '':
				return self.table_atributes[table]
			else:
				if atribute in self.table_atributes[table]:
					return self.table_atributes[table][atribute]
		return
	
	def get_date_column(self, land, table, increment=True):
		land = str(land).upper()
		table = str(table).upper()
		if increment:
			table = table + '_INC'
		if land in self.sql_tables:
			if table in self.sql_tables[land]:
				return self.sql_tables[land][table].get('date_col')
		return
		
	
	def get_sql_table(self, land, table, increment=False):
		"""
		returns sql db table name based on input:

		land -- sap land name, possible values: PMDI.sap_lands

		table -- sap table name, possible values: PMDI.sap_tables

		increment -- flag if incremental table is expected (default False)
		
		if increment: table += '_INC'
		
		return PMDI.sql_tables[land][table]['name']

		returns empty string if no setup found
		"""
		land = str(land).upper()
		table = str(table).upper()
		if land in self.sap_lands and table in self.sap_tables:
			if increment:
				table += '_INC'
			return self.sql_tables[land][table]['name']
		return
	
	def get_sql_table_connection(self, land, table, increment=False, format='str'):
		"""
		returns sql db table server and driver based on input:

		land -- sap land name, possible values: PMDI.sap_lands

		table -- sap table name, possible values: PMDI.sap_tables

		increment -- flag if incremental table is expected (default False)

		fortmat -- expected return format, if str then str, else dict (default: 'str')
		
		if increment: table += '_INC'
		
		return {'server': PMDI.sql_tables[land][table]['server'], 'driver': PMDI.sql_tables[land][table]['server']}

		returns empty dict/string if no setup found
		"""
		land = str(land).upper()
		table = str(table).upper()
		if land in self.sap_lands and table in self.sap_tables:
			if increment:
				table += '_INC'
			if format == 'str':
				return 'DRIVER={};SERVER={}'.format(
					self.sql_tables[land][table]['driver'],
					self.sql_tables[land][table]['server']
					)
			else:
				return {
					'server': self.sql_tables[land][table]['server'],
					'driver': self.sql_tables[land][table]['driver']
				}
		else:
			return
	
	def get_job_log_sql_table_connection(self, format='str'):
		"""
		returns connection details for job log table
		"""
		if self.sql_tables.get('LOG'):
			table_dict = dict(self.sql_tables.get('LOG').get('JOB_LOG'))
			if table_dict:
				if table_dict.get('name'):
					table_dict.pop('name')
				if table_dict.get('server') and table_dict.get('driver'):
					if format == 'str':
						return 'DRIVER={};SERVER={}'.format(
							table_dict.get('driver'),
							table_dict.get('server')
						)
					else:
						return table_dict
		return

	def get_job_log_sql_table(self):
		"""
		returns name of sql table which store job log
		"""
		if 'LOG' in self.sql_tables:
			if 'JOB_LOG' in self.sql_tables['LOG']:
				if 'name' in self.sql_tables['LOG']['JOB_LOG']:
					table = self.sql_tables['LOG']['JOB_LOG'].get('name')
					if type(table) == str:
						return table
		return
	
	def get_data_filter(self, table):
		table = str(table).upper()
		if table not in self.data_filters:
			return
		return self.data_filters[table]
	
	def get_numpy_schema(self, table):
		"""
		returns numpy schema for table

		table -- sap table name, possible values: PMDI.sap_tables
		
		returns {} if no setup found
		"""
		table = str(table).upper()
		if table not in self.sap_tables:
			return {}
		
		numpy_schema = {}
		for col in self.columns[table]:
			numpy_schema[col] = self.columns[table][col]['numpy_type']
		return numpy_schema
	
	def get_columns_list(self, table, prefix='', format='str', key_only=False):
		"""
		returns list of columns for given table.
		
		parameters:
		
		table -- sap table name, possible entries: PMDI.sap_tables
		
		prefix -- if populated, and format = 'str' columns names are listed with prefix: <prefix>.<column name>
		
		format -- if 'str' returns list of columns as string, else returns list (default 'str')
		
		key_only -- if True, return only key columns list
		"""
		table = str(table).upper()
		if prefix != '':
			prefix += '.'
		if table in self.sap_tables:
			if format == 'str':
				columns_list = ''
				for column in self.columns[table]:
					if key_only:
						if self.columns[table][column]['key']:
							if len(columns_list) > 0:
								columns_list += ', {}"{}"'.format(prefix, column)
							else:
								columns_list += '{}"{}"'.format(prefix, column)	
					else:
						if len(columns_list) > 0:
							columns_list += ', {}"{}"'.format(prefix, column)
						else:
							columns_list += '{}"{}"'.format(prefix, column)
				return columns_list
			else:
				columns_list = []
				for column in self.columns[table]:
					if key_only:
						if self.columns[table][column]['key']:
							columns_list.append(column)
					else:
						columns_list.append(column)
				return columns_list
		else:
			if format == 'str':
				return ''
			else:
				return []
	
	def get_next_page_sql_query(self, land, table, start_values={}, chunk = 100000):
		"""
		returns sql query to get next page results from DB
		
		land -- sap land, possible values: PMDI.sap_lands
		
		table -- sap table, possible values: PMDI.sap_tables

		start_values -- dictionary key columns values from last entry of previous page (default: {})

		chunk -- max size of expected return from SQL DB (default: 100000)

		returns empty string when one of:
		- land out of scope
		- table out of scope
		- start_values is not a dictionary
		"""
		table = str(table).upper()
		land = str(land).upper()
		if table not in self.sap_tables or land not in self.sap_lands or type(start_values) != dict:
			return ''
		
		sql_table = self.get_sql_table(land, table)
		key = self.get_columns_list(table=table, format='list',key_only=True)
		numpy_schema = self.get_numpy_schema(table)
		data_filter = self.get_data_filter(table)
		for k in key:
			if k not in start_values:
				if numpy_schema[k] == 'str':
					start_values[k] = '0'
				else:
					start_values[k] = 0
		val = start_values
		lastk = len(key)
		query = 'with pg as (\n\tSELECT TOP {} {}\n'.format(chunk, self.get_columns_list(table,key_only=True))
		query += '\t\tFROM {} \n\t\tWHERE \n'.format(sql_table)
		if data_filter != '':
			query += '\t\t\t{} and\n'.format(data_filter)
		query += '\t\t\t(\n'
		i = 0
		for k in key:
			if i == 0:
				query += '\t\t\t\t('
			else:
				query += '\t\t\t\tor ('
			j = 1
			for k1 in key:
				if j <= lastk - i:
					if numpy_schema[k1] == 'str':
						v = '\'{}\''.format(str(val[k1]))
					else:
						v = '{}'.format(str(val[k1]))
					if j < lastk - i:
						query += '"{}" = {} and '.format(k1,v)
					else:
						query += '"{}" > {}'.format(k1,v)
				j += 1
			query += ')\n'
			i += 1
		query += '\t\t\t)\n'
		query += '\t\torder by {}\n'.format(self.get_columns_list(table,key_only=True))
		query += '\t)\n\tselect {}\n'.format(self.get_columns_list(table,prefix='tab'))
		query += '\t\tfrom {} as tab\n\t\tjoin pg on\n'.format(sql_table)
		i = 1
		for k in key:
			if i == 1:
				query += '\t\t\ttab."{}" = pg."{}"\n'.format(k, k)
			else:
				query += '\t\t\tand tab."{}" = pg."{}"\n'.format(k, k)
			i += 1
		query += '\t\torder by {}\n'.format(self.get_columns_list(table,prefix='tab', key_only=True))

		return query