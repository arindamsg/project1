from pmdi import PMDI
from celonisapi import celonisapi
import os
import datetime
import pyodbc
import pandas as pd
import pyarrow
import pyarrow.parquet
import re
import time
import shutil
import psycopg2
import uuid
from sqlalchemy import create_engine
class PMDIExtractor:
	
	pmdi = PMDI('')
	land = ''
	table = ''
	start_time = None
	log_file_name = ''
	sql_conn = None
	custom_extractors = {}
	postgresql_cur = None
	postgre_conn = None
	engine = None
	postgresql_host = None
	postgresql_port = None
	postgresql_db = None
	postgresql_user = None
	postgresql_pwd = None
	retention = "'30'"
	exec_id = None

	def __init__(self, land, table, env='mvp'):
		self.pmdi = PMDI(str(env).lower())
		self.postgresql_host = self.pmdi.postgresql_host
		self.postgresql_port = self.pmdi.postgresql_port
		self.postgresql_db = self.pmdi.postgresql_db
		self.postgresql_user = self.pmdi.postgresql_user
		self.postgresql_pwd = self.pmdi.postgresql_pwd
		self.retention = self.pmdi.retention
		self.exec_id = str(uuid.uuid4())
		self.land = str(land).upper()
		self.table = str(table).upper()
		self.start_time = datetime.datetime.now()
		self.log_file_name = './logs/PMDIExtractor/{}_{}_{}.log'.format(self.land, self.table,str(self.start_time)).replace('-','').replace(':','').replace(' ','_')
		path = './logs'
		self.mkdir_if_not_exist(path)
		path += '/PMDIExtractor'
		self.mkdir_if_not_exist(path)
		f = open(self.log_file_name,'a+')
		f.write('Time\tRun Time\tType\tLog\n')
		f.close()
		self.custom_extractors = {
			'ABC': self.extract_full_cdpos,
			'XYZ': self.extract_full_bseg
		}
		self.log('PMDIExtractor for {} {} initialized'.format(self.land, self.table))
	
	#common methods
	def log(self,message='',mtype='I'):
		f = open(self.log_file_name,'a+')
		msg = '{}\t{}\t{}\t{}'.format(datetime.datetime.now(), datetime.datetime.now()-self.start_time,str(mtype),str(message))
		f.write('{}\n'.format(msg))
		f.close()
		print(msg)
		return datetime.datetime.now()

	def init_sql(self, cdetails=None):
		if not cdetails:
			cdetails = self.pmdi.get_sql_table_connection(self.land, self.table)
		print (cdetails)
		if cdetails == None:
			self.log(
				'connection details for {} {} not available in config'.format(
					self.land, 
					self.table),
				'E')
			self.sql_conn = None
			return
		self.sql_conn = pyodbc.connect(cdetails)
		return True

	def init_postgre_engine(self):
		conn= "postgresql://" + self.postgresql_user + ":" + self.postgresql_pwd + "@" 
		conn+= self.postgresql_host + ":" + self.postgresql_port + "/" + self.postgresql_db
		#self.engine = create_engine('postgresql://postgres:admin@127.0.0.1:5432/test')
		self.engine = create_engine(conn)


		return 

	def close_postgre_engine(self):
		self.engine = None
		return True

	def init_postgre(self):
		self.postgre_conn = psycopg2.connect(database = self.postgresql_db, user = self.postgresql_user, password = self.postgresql_pwd, host = self.postgresql_host, port = self.postgresql_port)
		self.postgresql_cur = self.postgre_conn.cursor()
		return

	def close_postgre(self):
		if not self.postgre_conn:
			return True
		else:
			self.postgre_conn.close()
			self.postgre_conn = None
			self.postgresql_cur = None
			return True

	def write_postgrelog(self, envt, env, opt):
		self.init_postgre()
		qr="Insert into dbo.LOG values('" + self.exec_id + "', NOW(),'" + envt + "','" + self.land + "','" + self.table + "','" + env + "','" + opt + "')"
		self.postgresql_cur.execute(qr)
		self.postgre_conn.commit()
		self.close_postgre()

	def retention_full(self):
		self.init_postgre()
		qr = "Truncate Table " + self.get_postgre_table_RET()
		self.postgresql_cur.execute(qr)
		#self.postgre_conn.commit()
		#self.close_postgre()
		#self.init_postgre()
		qr = "INSERT INTO " + self.get_postgre_table_RET() + " SELECT *, NOW() FROM " + self.get_postgre_table()
		self.postgresql_cur.execute(qr)
		self.postgre_conn.commit()
		self.close_postgre()

	def retention_inc(self):
		self.init_postgre()
		qr = "INSERT INTO " + self.get_postgre_table_RET() + " SELECT *, NOW() FROM " + self.get_postgre_table()
		self.postgresql_cur.execute(qr)
		#self.postgre_conn.commit()
		#self.close_postgre()
		#self.init_postgre()
		qr = "DELETE FROM " + self.get_postgre_table_RET() + " WHERE DATE_PART('day', NOW()-created_dt) >= " + self.retention  
		self.postgresql_cur.execute(qr)
		self.postgre_conn.commit()
		self.close_postgre()


	def close_sql(self):
		if not self.sql_conn:
			return True
		else:
			self.sql_conn.close()
			self.sql_conn = None
			return True

	def mkdir_if_not_exist(self,path):
		if not(os.path.exists(path) and os.path.isdir(path)):
			os.mkdir(path)
			self.log('dir {} created'.format(path))
	
	def rmdir_if_exist(self, path):
		if os.path.exists(path) and os.path.isdir(path):
			shutil.rmtree(path)
			self.log('dir {} removed'.format(path))
	
	def log_statistics(self, times):
		if 'Name' in times:
			self.log(times['Name'])
		for task in times:
			if type(times[task]) == list and len(times[task]) > 0:
				deltasum = datetime.timedelta()
				for t in times[task]:
					deltasum += t
				average = deltasum / len(times[task])
				self.log('{}({}): min: {}, max:{}, avg: {}, total: {}'.format(
					task,
					len(times[task]),
					min(times[task]),
					max(times[task]),
					average,
					deltasum
				))
	
	def get_sql(self, function, vars):
		path = './sql/{}.sql'.format(str(function))
		if not(os.path.exists(path) or os.path.isfile(path)):
			self.log('file {} not found'.format(path), 'E')
			return
		f = open(path,'r')
		query = f.readlines()
		query = ''.join(query)
		for v in vars:
			var = '@' + v
			query = query.replace(var,str(vars[v]))
		check = list(set(re.findall(r'@\w+', query)))
		if len(check) > 0:
			self.log('valuas not provided for variables: {}'.format(check),'E')
			return
		return query
	
	#FULL load
	#full extract sql to parquet methods
	def extract_full_bseg(self, path):
		now = datetime.datetime.now
		parquet_f_name = path + '/{}.parquet'

		next_page_fun = self.pmdi.get_table_atribute(self.table, atribute='next_page_fun')
		if next_page_fun == None:
			self.log('table atribute next_page_fun not found in config','E')
			return
		
		next_match_fun = self.pmdi.get_table_atribute(self.table, atribute='next_match_fun')
		if next_page_fun == None:
			self.log('table atribute next_match_fun not found in config','E')
			return
		
		next_bukrs_fun = self.pmdi.get_table_atribute(self.table, atribute='next_bukrs_fun')
		if next_page_fun == None:
			self.log('table atribute next_bukrs_fun not found in config','E')
			return
		
		constants = {
			'CHUNK': 500000,
			'BSEG_TAB': self.pmdi.get_sql_table(self.land, self.table)
		}
		if constants['BSEG_TAB'] == None:
			self.log('not possible to get value for BSEG_TAB','E')
			return
		
		iteration = 1
		total = 0
		times = {
			'Name': 'Statistics for full extract of {} {}'.format(self.land, self.table),
			'Iteration Execution': [],
			'SQL Execution': [],
			'Schema Update Execution': [],
			'Pyarrow Table Generation Execution': [],
			'Parquet file Generation Execution': []
		}
		
		start_values = dict(constants)
		start_values['BUKRS'] = '0'
		start_values['BELNR'] = '0'
		start_values['GJAHR'] = 2017
		start_values['BUZEI'] = 0

		query = self.get_sql(next_page_fun, start_values)
		while True:
			iter_start_time = now()
			log_message = 'Iteration {}: '.format(iteration)
			self.log('{}starts for: {}'.format(log_message,start_values))
			
			timestamp = self.log('{}execute SQL query'.format(log_message))
			self.init_sql()
			if not self.sql_conn:
				self.log('no connection to SQL','E')
				return
			df = pd.read_sql(query, self.sql_conn)
			self.close_sql()
			delta = now()-timestamp
			times['SQL Execution'].append(delta)
			self.log('{}done in {}'.format(log_message, delta))

			extracted = len(df)
			if extracted > 0:
				last_row = df.tail(1).to_dict('records')[0]
				if last_row['KOART'] not in ['D','K'] or last_row['GJAHR'] < 2107:
					df = df[:-1]
					extracted = len(df)
				if extracted > 0:
					#save data
					total += extracted
					timestamp = self.log('{}update numpy schema'.format(log_message))
					df = df.astype(self.pmdi.get_numpy_schema(self.table))
					delta = now()-timestamp
					times['Schema Update Execution'].append(delta)
					self.log('{}done in {}'.format(log_message, delta))
					
					timestamp = self.log('{}generate pyarrow table'.format(log_message))
					pyarrow_table = pyarrow.Table.from_pandas(df, preserve_index=False)
					delta = now()-timestamp
					times['Pyarrow Table Generation Execution'].append(delta)
					self.log('{}done in {}'.format(log_message, delta))
					
					fname = parquet_f_name.format(iteration)
					timestamp = self.log('{}save parquet file {}'.format(
						log_message,
						fname
					))
					pyarrow.parquet.write_table(pyarrow_table, 
						fname, 
						use_deprecated_int96_timestamps=True)
					delta = now()-timestamp
					times['Parquet file Generation Execution'].append(delta)
					self.log('{}done in {}'.format(log_message, delta))

					timestamp = self.log('{}clear memory'.format(log_message))
					del df
					del pyarrow_table
					self.log('{}done in {}'.format(log_message, now()-timestamp))
					start_values = self.prepare_start_values(last_row)
					for c in constants:
						start_values[c] = constants[c]
					query = self.get_sql(next_page_fun,start_values)
					delta = now()-iter_start_time
					times['Iteration Execution'].append(delta)
					self.log('{}complete in {} extracted {} rows, in total {}'.format(
						log_message,
						delta,
						extracted,
						total
					))
				else:
					#get next match
					self.log('{}no data found, get next match'.format(log_message))
					query = query = self.get_sql(next_match_fun,start_values)
					timestamp = self.log('{}execute SQL query'.format(log_message))
					self.init_sql()
					if not self.sql_conn:
						self.log('no connection to SQL','E')
						return
					df = pd.read_sql(query, self.sql_conn)
					self.close_sql()
					delta = now()-timestamp
					times['SQL Execution'].append(delta)
					self.log('{}done in {}'.format(log_message, delta))

					if len(df) > 0:
						last_row = df.tail(1).to_dict('records')[0]
						start_values = self.prepare_start_values(last_row)
						for c in constants:
							start_values[c] =constants[c]
						query = self.get_sql(next_page_fun,start_values)
						delta = now()-iter_start_time
						times['Iteration Execution'].append(delta)
						self.log('{}complete in {} extracted {} rows, in total {}'.format(
							log_message,
							delta,
							extracted,
							total
						))
					else:
						self.log('{}no more data'.format(log_message))
						delta = now()-iter_start_time
						times['Iteration Execution'].append(delta)
						self.log('{}complete in {} extracted {} rows, in total {}'.format(
							log_message,
							delta,
							extracted,
							total
						))
						break
					del df
			else:
				#no data found for bukrs
				self.log('{}no more data found for {}'.format(log_message, start_values['BUKRS']))
				query = self.get_sql(next_bukrs_fun,start_values)
				timestamp = self.log('{}execute SQL query'.format(log_message))
				self.init_sql()
				if not self.sql_conn:
					self.log('no connection to SQL','E')
					return
				df = pd.read_sql(query, self.sql_conn)
				self.close_sql()
				delta = now()-timestamp
				times['SQL Execution'].append(delta)
				self.log('{}done in {}'.format(log_message, delta))

				if len(df) > 0:
					last_row = df.tail(1).to_dict('records')[0]
					start_values = self.prepare_start_values(last_row)
					for c in constants:
						start_values[c] =constants[c]
					query = self.get_sql(next_page_fun,start_values)
					delta = now()-iter_start_time
					times['Iteration Execution'].append(delta)
					self.log('{}complete in {} extracted {} rows, in total {}'.format(
						log_message,
						delta,
						extracted,
						total
					))
				else:
					self.log('{}no more data'.format(log_message))
					delta = now()-iter_start_time
					times['Iteration Execution'].append(delta)
					self.log('{}complete in {} extracted {} rows, in total {}'.format(
						log_message,
						delta,
						extracted,
						total
					))
					break
				del df
			iteration += 1

	def extract_full_cdpos(self, path):
		now = datetime.datetime.now
		parquet_f_name = path + '/{}.parquet'
		oclasses = self.pmdi.get_table_atribute(self.table, atribute='obj_classes')
		if oclasses == None:
			self.log('table atribute obj_classes not found in config','E')
			return
		next_page_fun = self.pmdi.get_table_atribute(self.table, atribute='next_page_fun')
		if next_page_fun == None:
			self.log('table atribute next_page_fun not found in config','E')
			return
		constants = {
			'CDHDR_TAB': self.pmdi.get_sql_table(self.land, 'CDHDR'),
			'CDPOS_TAB': self.pmdi.get_sql_table(self.land, self.table)
		}
		for const in constants:
			if constants[const] == None:
				self.log('not possible to get value for {}'.format(const),'E')
				return
		iteration = 1
		total = 0
		times = {
			'Name': 'Statistics for full extract of {} {}'.format(self.land, self.table),
			'Iteration Execution': [],
			'SQL Execution': [],
			'Schema Update Execution': [],
			'Pyarrow Table Generation Execution': [],
			'Parquet file Generation Execution': []
		}
		for oclass in oclasses:
			chunk = 0
			if 'chunk' in oclasses[oclass]:
				chunk = oclasses[oclass]['chunk']
			else:
				self.log('chunk not configured for object class {}'.format(oclass),'E')
				continue
			start_values = dict(constants)
			start_values['CHUNK'] = chunk
			start_values['OBJECTCLAS'] = oclass
			start_values['CHANGENR'] = '0'
			query = self.get_sql(next_page_fun,start_values)
			while True:
				iter_start_time = now()
				log_message = 'Iteration {}: '.format(iteration)
				self.log('{}starts for: {}'.format(log_message,start_values))
				
				timestamp = self.log('{}execute SQL query'.format(log_message))
				self.init_sql()
				if not self.sql_conn:
					self.log('no connection to SQL','E')
					return
				df = pd.read_sql(query, self.sql_conn)
				self.close_sql()
				delta = now()-timestamp
				times['SQL Execution'].append(delta)
				self.log('{}done in {}'.format(log_message, delta))

				extracted = len(df)
				total += extracted
				
				if extracted == 0:
					delta = now()-iter_start_time
					times['Iteration Execution'].append(delta)
					self.log('{}complete in {} extracted {} rows, in total {}'.format(
						log_message,
						delta,
						extracted,
						total
					))
					iteration += 1
					break
				
				last_row = df.tail(1).to_dict('records')[0]

				timestamp = self.log('{}update numpy schema'.format(log_message))
				df = df.astype(self.pmdi.get_numpy_schema(self.table))
				delta = now()-timestamp
				times['Schema Update Execution'].append(delta)
				self.log('{}done in {}'.format(log_message, delta))
				
				timestamp = self.log('{}generate pyarrow table'.format(log_message))
				pyarrow_table = pyarrow.Table.from_pandas(df, preserve_index=False)
				delta = now()-timestamp
				times['Pyarrow Table Generation Execution'].append(delta)
				self.log('{}done in {}'.format(log_message, delta))
				
				fname = parquet_f_name.format(iteration)
				timestamp = self.log('{}save parquet file {}'.format(
					log_message,
					fname
				))
				pyarrow.parquet.write_table(pyarrow_table, 
					fname, 
					use_deprecated_int96_timestamps=True)
				delta = now()-timestamp
				times['Parquet file Generation Execution'].append(delta)
				self.log('{}done in {}'.format(log_message, delta))

				timestamp = self.log('{}clear memory'.format(log_message))
				del df
				del pyarrow_table
				self.log('{}done in {}'.format(log_message, now()-timestamp))
				start_values['CHANGENR'] = last_row['CHANGENR']
				query = self.get_sql(next_page_fun,start_values)
				delta = now()-iter_start_time
				times['Iteration Execution'].append(delta)
				self.log('{}complete in {} extracted {} rows, in total {}'.format(
					log_message,
					delta,
					extracted,
					total
				))
				iteration += 1
		self.log('Full extract completed for {} {}'.format(self.land, self.table))
		self.log_statistics(times)

	def get_postgre_query(self, chunk, max):
		query = 'Select * from ' + self.get_postgre_table() 
		#+ ' where CAST("BELNR"  as INT) > CAST(' + max +  ' as INT)'
		#query += ' Order By "BELNR" '
		#query += ' Limit CAST(' + chunk + ' as INT)'
		return query
	
	def get_postgre_table(self):
		#table = self.postgresql_db + "." + self.land + "." + self.table
		table = self.postgresql_db + ".dbo" + "." + self.table

		return table
	
	def get_postgre_table_RET(self):
		#table = self.postgresql_db + "." + self.land + "." + self.table + "_RET"
		table = self.postgresql_db + ".dbo" + "." + self.table + "_RET"

		return table


	
	def extract_full(self):
		now = datetime.datetime.now
		self.log('Full extract started for {} {}'.format(self.land, self.table))
		path = './parquet'
		self.mkdir_if_not_exist(path)
		path += '/full'
		self.mkdir_if_not_exist(path)
		path += '/{}'.format(self.land)
		self.mkdir_if_not_exist(path)
		path += '/{}'.format(self.table)
		self.mkdir_if_not_exist(path)
		path += '/{}'.format(self.start_time).replace('-','').replace(':','').replace(' ','_')
		self.mkdir_if_not_exist(path)
		parquet_f_name = path + '/{}.parquet'

		if self.table in self.custom_extractors:
			self.custom_extractors[self.table](path)
		else:
			iteration = 1
			chunk = 4
			start_values = {}
			total = 0
			maxx = 0
			times = {
				'Name': 'Statistics for full extract of {} {}'.format(self.land, self.table),
				'Iteration Execution': [],
				'SQL Execution': [],
				'Schema Update Execution': [],
				'Pyarrow Table Generation Execution': [],
				'Parquet file Generation Execution': []
			}
			#query = self.pmdi.get_next_page_sql_query(
			#	self.land,
			#	self.table,
			#	start_values=start_values,
			#	chunk=chunk)
			
			while True:
				query = self.get_postgre_query(str(chunk),str(maxx))
				iter_start_time = now()
				log_message = 'Iteration {}: '.format(iteration)
				self.log('{}starts for: {}'.format(log_message,start_values))
				
				timestamp = self.log('{}execute SQL query'.format(log_message))
				self.init_postgre_engine()
				df = pd.read_sql(query, self.engine)
				#maxx = df["BELNR"].max()
				last_row = df.tail(1).to_dict('records')[0]
				self.close_postgre_engine()
				delta = now()-timestamp
				times['SQL Execution'].append(delta)
				self.log('{}done in {}'.format(log_message, delta))
				
				extracted = len(df)
				total += extracted
				
				if extracted == 0:
					delta = now()-iter_start_time
					times['Iteration Execution'].append(delta)
					self.log('{}complete in {} extracted {} rows, in total {}'.format(
						log_message,
						delta,
						extracted,
						total
					))
					break

				timestamp = self.log('{}update numpy schema'.format(log_message))
				df = df.astype(dtype=self.pmdi.get_numpy_schema(self.table))
				delta = now()-timestamp
				times['Schema Update Execution'].append(delta)
				self.log('{}done in {}'.format(log_message, delta))
				
				timestamp = self.log('{}generate pyarrow table'.format(log_message))
				pyarrow_table = pyarrow.Table.from_pandas(df, preserve_index=False)
				delta = now()-timestamp
				times['Pyarrow Table Generation Execution'].append(delta)
				self.log('{}done in {}'.format(log_message, delta))
				
				fname = parquet_f_name.format(iteration)
				timestamp = self.log('{}save parquet file {}'.format(
					log_message,
					fname
				))
				pyarrow.parquet.write_table(pyarrow_table, 
					fname, 
					use_deprecated_int96_timestamps=True)
				delta = now()-timestamp
				times['Parquet file Generation Execution'].append(delta)
				self.log('{}done in {}'.format(log_message, delta))

				timestamp = self.log('{}clear memory'.format(log_message))
				del df
				del pyarrow_table
				self.log('{}done in {}'.format(log_message, now()-timestamp))

				if extracted >= chunk:
					delta = now()-iter_start_time
					times['Iteration Execution'].append(delta)
					self.log('{}complete in {} extracted {} rows, in total {}'.format(
						log_message,
						delta,
						extracted,
						total
					))
					break
				
				start_values = self.prepare_start_values(last_row)
				query = self.pmdi.get_next_page_sql_query(
					self.land,
					self.table,
					start_values=start_values,
					chunk=chunk)
				delta = now()-iter_start_time
				times['Iteration Execution'].append(delta)
				self.log('{}complete in {} extracted {} rows, in total {}'.format(
					log_message,
					delta,
					extracted,
					total
				))
				iteration += 1
			self.log('Full extract completed for {} {}'.format(self.land, self.table))
			self.log_statistics(times)
	
	def prepare_start_values(self, last_row):
		key_columns = self.pmdi.get_columns_list(self.table, 
			format='dict', 
			key_only=True)
		start_values = {}
		for k in key_columns:
			if k in last_row:
				start_values[k] = last_row[k]
		return start_values

	#full export parquet to celonis mothods	
	def export_full(self):
		self.log('Full export for {} {}'.format(self.land, self.table))
		celonis_config_check = [
			'tenant',
			'cluster',
			'api_key',
			'pool_id',
			'connection_id']
		found = True
		for var in celonis_config_check:
			if self.pmdi.celonis_config.get(var) == None:
				found = False
				self.log('{} missing for celonis config'.format(var), 'E')
		if not found:
			return

		#root path for table
		root_path = './parquet/full/{}/{}'.format(self.land, self.table)
		#path for current execution
		date_path = '{}'.format(self.start_time).replace('-','').replace(':','').replace(' ','_')
		path = root_path + '/{}'.format(date_path)
		
		if not (os.path.exists(path) and os.path.isdir(path)):
			#path for current execution does not exist
			found = False
			if os.path.exists(root_path) and os.path.isdir(root_path):
				#root path exist select last folder
				dirs = os.listdir(root_path)
				i = len(dirs) - 1
				while i >= 0:
					date_path = dirs[i]
					path = root_path + '/{}'.format(date_path)
					if os.path.isdir(path):
						found = True
						break
					i -= 1
			if not found:
				#no data export
				self.log('No data exported for {} {}. Please run extract_full first'.format(
					self.land,
					self.table
				), 'W')
				return
		arch_path = './parquet/full/archive'
		self.mkdir_if_not_exist(arch_path)
		arch_path += '/{}'.format(self.land)
		self.mkdir_if_not_exist(arch_path)
		arch_path += '/{}'.format(self.table)
		self.mkdir_if_not_exist(arch_path)
		arch_path += '/{}'.format(date_path)
		self.rmdir_if_exist(arch_path)
		self.mkdir_if_not_exist(arch_path)
		self.log('Start export from path {}'.format(path))
		uppie = celonisapi(
			tenant=self.pmdi.celonis_config.get('tenant'),
			realm=self.pmdi.celonis_config.get('cluster'),
			api_key=self.pmdi.celonis_config.get('api_key')
		)
		self.log('Create job in Celonis')
		jobhandle = uppie.create_job(
			pool_id=self.pmdi.celonis_config.get('pool_id'),
			data_connection_id=self.pmdi.celonis_config.get('connection_id'),
			targetName='{}_{}'.format(
				self.land, self.table
			),
			upsert=True
		)
		if jobhandle.get('id'):
			self.log('job created with id {}'.format(jobhandle.get('id')))
			self.log('push files to job')
			uppie.push_new_dir(
				pool_id=self.pmdi.celonis_config.get('pool_id'),
				job_id=jobhandle.get('id'),
				dir_path=path,
				print=self.log
			)
			self.log('submit job')
			uppie.submit_job(
				pool_id=self.pmdi.celonis_config.get('pool_id'),
				job_id=jobhandle.get('id')
			)
			job_find = False
			job_done = False
			while True:
				time.sleep(5)
				jobs = uppie.list_jobs(
					pool_id=self.pmdi.celonis_config.get('pool_id')
				)
				for job in jobs:
					if job['id'] == jobhandle['id']:
						job_find = True
						if job['status'] == 'DONE':
							self.log('job finished with success')
							for log in job['logs']:
								self.log(log)
							job_done = True
						elif job['status'] == 'ERROR':
							self.log('error occured','E')
							for log in job['logs']:
								self.log(log,'E')
							job_done = True
						else:
							self.log('job in progress')
						break
				if job_done == True:
					uppie.delete_job(
						pool_id=self.pmdi.celonis_config.get('pool_id'), 
						job_id=jobhandle['id']
					)
				if job_find == False or job_done == True:
					break
			self.log('move files to archive')
			sfiles = list(filter(lambda f: f.endswith('.parquet'), [path + '/{}'.format(f) for f in os.listdir(path)]))
			afiles = list(filter(lambda f: f.endswith('.parquet'), [arch_path + '/{}'.format(f) for f in os.listdir(path)]))
			count = len(sfiles)
			i = 0
			print('{}/{}'.format(i,count),end='')
			while i < count:
				shutil.move(sfiles[i],afiles[i])
				i += 1
				print('\r{}/{}'.format(i,count),end='')
			print(' done')
			self.rmdir_if_exist(path)
			self.log('done')

		else:
			self.log(jobhandle, 'E')

	#DELTA load
	#delta extract sql to parquet
	def get_extract_dates_range(self):
		job_name = self.pmdi.get_job_name(
			jtype= 'delta',
			land= self.land)
		if not job_name:
			self.log('could not find job name','E')
			return {}
		job_log = {
			'JOB_LOG_TAB': self.pmdi.get_job_log_sql_table(),
			'JOB_NAME': job_name
		}
		get_last_dates_fun = self.pmdi.get_table_atribute('JOB_LOG','get_last_dates_fun')
		if not get_last_dates_fun:
			self.log('table atribute next_page_fun not found in config','E')
			return {}
		query = self.get_sql(get_last_dates_fun, job_log)

		self.init_sql(self.pmdi.get_job_log_sql_table_connection())
		df = pd.read_sql(query, self.sql_conn)
		self.close_sql()

		if type(df) == pd.DataFrame:
			return {
				'start': datetime.date.fromisoformat(df['EXT_START_DATE'][0]),
				'end': datetime.date.fromisoformat(df['EXT_END_DATE'][0])
			}
		return {}
	
	def get_extract_size(self, date, times):
		now = datetime.datetime.now
		#get table variables
		table_name = self.pmdi.get_sql_table(self.land, self.table, increment=True)
		date_column = self.pmdi.get_date_column(self.land, self.table, increment=True)
		table_filter = self.pmdi.get_data_filter(self.table)

		variables = {
			'TAB_NAME': table_name,
			'DATE_COL': date_column,
			'DATE': date,
			'FILTER': table_filter
		}

		#get func name
		inc_count_fun = self.pmdi.get_table_atribute(self.table, 'inc_count_fun')

		#prepare query
		query = self.get_sql(inc_count_fun, variables)

		#get connection details
		cdetails = self.pmdi.get_sql_table_connection(self.land, self.table, increment=True)
		
		start = self.log('get rows count')
		self.init_sql(cdetails=cdetails)
		df = pd.read_sql(query, self.sql_conn)
		self.close_sql()
		delta = now()-start
		self.log('done in {}'.format(delta))
		times['SQL Execution'].append(delta)


		if type(df) == pd.DataFrame:
			return [df['count'][0], times]
		return [0, times]
	
	def extract_day(self, date, times):
		now = datetime.datetime.now
		
		self.log('extract for {} {} on {} started'.format(self.land, self.table, date))
		path = './parquet'
		self.mkdir_if_not_exist(path)
		path += '/delta'
		self.mkdir_if_not_exist(path)
		path += '/{}'.format(self.land)
		self.mkdir_if_not_exist(path)
		path += '/{}'.format(self.table)
		self.mkdir_if_not_exist(path)
		path += '/{}'.format(date.strftime('%Y-%m-%d'))
		self.mkdir_if_not_exist(path)
		parquet_f_name = path+'/{}.parquet'
		
		count, times = self.get_extract_size(date, times)
		offset = 0
		load = 100000
		iteration = 1
		self.log('number of entries to be extracted: {}'.format(count))
		self.log('expected number of iterations: {}'.format(
			int(count/load)+1 if count%load > 0 else count/load
		))

		if count > 0:
			#get table variables
			columns = self.pmdi.get_columns_list(self.table)
			key_columns = self.pmdi.get_columns_list(self.table, key_only=True)
			table_name = self.pmdi.get_sql_table(self.land, self.table, increment=True)
			date_column = self.pmdi.get_date_column(self.land, self.table, increment=True)
			table_filter = self.pmdi.get_data_filter(self.table)

			variables = {
				'COLUMNS' : columns,
				'TAB_NAME': table_name,
				'DATE_COL': date_column,
				'DATE': date,
				'FILTER': table_filter,
				'KEY_COLUMNS': key_columns,
				'OFFSET': offset,
				'LOAD': load
			}

			#get func name
			inc_count_fun = self.pmdi.get_table_atribute(self.table, 'inc_page_fun')

			#prepare query
			query = self.get_sql(inc_count_fun, variables)

			#get connection details
			cdetails = self.pmdi.get_sql_table_connection(self.land, self.table, increment=True)
			log_str_tmp = 'Iteration {}: '
			while True:
				log_str = log_str_tmp.format(iteration) + '{}'
				istart = now()
				#get data
				start = self.log(log_str.format('execute sql query'))
				self.init_sql(cdetails=cdetails)
				df = pd.read_sql(query, self.sql_conn)
				self.close_sql()
				delta = now()-start
				self.log(log_str.format('done in {}'.format(delta)))
				times['SQL Execution'].append(delta)

				#update schema
				start = self.log(log_str.format('update schema'))
				df = df.astype(dtype=self.pmdi.get_numpy_schema(self.table))
				delta = now()-start
				self.log(log_str.format('done in {}'.format(delta)))
				times['Schema Update Execution'].append(delta)

				#create pyarrow table
				start = self.log(log_str.format('create pyarrow table'))
				pyarrow_table = pyarrow.Table.from_pandas(df, preserve_index=False)
				delta = now()-start
				self.log(log_str.format('done in {}'.format(delta)))
				times['Pyarrow Table Generation Execution'].append(delta)

				#save file
				fname = parquet_f_name.format(iteration)
				start = self.log(log_str.format('save {}'.format(fname)))
				pyarrow.parquet.write_table(
					pyarrow_table,
					fname,
					use_deprecated_int96_timestamps=True
				)
				delta = now()-start
				self.log(log_str.format('done in {}'.format(delta)))
				times['Parquet file Generation Execution'].append(delta)

				size = len(df)
				
				#clear memory
				del df
				del pyarrow_table

				#check if end of data
				if size < load:
					delta = now() - istart
					self.log(log_str.format('completed in {}'.format(delta)))
					times['Iteration Execution'].append(delta)
					break
				
				#prepare next iteration
				variables['OFFSET'] = variables['OFFSET'] + load
				query = self.get_sql(inc_count_fun, variables)
				delta = now() - istart
				self.log(log_str.format('completed in {}'.format(delta)))
				times['Iteration Execution'].append(delta)
				iteration += 1

		self.log('extract for {} {} on {} done'.format(self.land, self.table, date))
		return times
	
	def extract_days(self, start, end, times):
		date = start
		delta = datetime.timedelta(days=1)
		while date <= end:
			times = self.extract_day(date, times)
			date = date + delta
		return times

	def extract_last_job(self):
		timestamp = self.log('Delta Extract for {} {} started'.format(
			self.land, 
			self.table
		))
		times = {
			'Name': 'Statistics for delta extract of {} {}'.format(self.land, self.table),
			'Iteration Execution': [],
			'SQL Execution': [],
			'Schema Update Execution': [],
			'Pyarrow Table Generation Execution': [],
			'Parquet file Generation Execution': []
		}
		dates = self.get_extract_dates_range()
		self.log('Last job dates range {} - {}'.format(
			dates.get('start'),
			dates.get('end')
		))
		times = self.extract_days(
			dates.get('start'),
			dates.get('end'),
			times
		)
		delta = datetime.datetime.now() - timestamp
		self.log('Delta Extract for {} {} done in {}'.format(
			self.land, 
			self.table,
			delta
		))
		self.log_statistics(times)
	#export to celonis
	def export_last_job(self):
		timestamp = self.log('Delta Export for {} {} started'.format(
			self.land, 
			self.table
		))
		
		path_tmp = './parquet/delta/{}/{}/{}'
		day_delta = datetime.timedelta(days=1)
		
		#get last job date range
		dates = self.get_extract_dates_range()
		self.log('Last job dates range {} - {}'.format(
			dates.get('start'),
			dates.get('end')
		))
		#check if folders exists
		date = dates.get('start')
		found = True
		while date <= dates.get('end'):
			path = path_tmp.format(self.land, self.table, date.strftime('%Y-%m-%d'))
			if not (os.path.exists(path) and os.path.isdir(path)):
				found = False
				break
			date = date + day_delta
		if not found:
			self.log('No data exported for {} {}. Please run extract_last_job first'.format(
				self.land,
				self.table
			), 'W')
			return
		#create path to archive processed files
		arch_path_tmp = './parquet/delta/archive'
		self.mkdir_if_not_exist(arch_path_tmp)
		arch_path_tmp += '/{}'.format(self.land)
		self.mkdir_if_not_exist(arch_path_tmp)
		arch_path_tmp += '/{}'.format(self.table)
		self.mkdir_if_not_exist(arch_path_tmp)


		date = dates.get('start')
		#init celonis api
		uppie = celonisapi(
			tenant=self.pmdi.celonis_config.get('tenant'),
			realm=self.pmdi.celonis_config.get('cluster'),
			api_key=self.pmdi.celonis_config.get('api_key')
		)
		self.log('Create job in Celonis')
		jobhandle = uppie.create_job(
			pool_id=self.pmdi.celonis_config.get('pool_id'),
			data_connection_id=self.pmdi.celonis_config.get('connection_id'),
			targetName='{}_{}'.format(
				self.land, self.table
			),
			upsert=True
		)
		if jobhandle.get('id'):
			self.log('job created with id {}'.format(jobhandle.get('id')))
			self.log('push files to job')
			while date <= dates.get('end'):
				path = path_tmp.format(self.land, self.table, date.strftime('%Y-%m-%d'))
				uppie.push_new_dir(
					pool_id=self.pmdi.celonis_config.get('pool_id'),
					job_id=jobhandle.get('id'),
					dir_path=path,
					print=self.log
				)
				date = date + day_delta
			self.log('submit job')
			uppie.submit_job(
				pool_id=self.pmdi.celonis_config.get('pool_id'),
				job_id=jobhandle.get('id')
			)
			job_find = False
			job_done = False
			while True:
				time.sleep(5)
				jobs = uppie.list_jobs(
					pool_id=self.pmdi.celonis_config.get('pool_id')
				)
				for job in jobs:
					if job['id'] == jobhandle['id']:
						job_find = True
						if job['status'] == 'DONE':
							self.log('job finished with success')
							for log in job['logs']:
								self.log(log)
							job_done = True
						elif job['status'] == 'ERROR':
							self.log('error occured','E')
							for log in job['logs']:
								self.log(log,'E')
							job_done = True
						else:
							self.log('job in progress')
						break
				if job_done == True:
					uppie.delete_job(
						pool_id=self.pmdi.celonis_config.get('pool_id'), 
						job_id=jobhandle['id']
					)
				if job_find == False or job_done == True:
					break
			self.log('move files to archive')
			arch_path_tmp = './parquet/delta/archive/{}/{}/{}'
			date = dates.get('start')
			sfiles = []
			afiles = []
			while date <= dates.get('end'):
				path = path_tmp.format(self.land, self.table, date.strftime('%Y-%m-%d'))
				arch_path = arch_path_tmp.format(self.land, self.table, date.strftime('%Y-%m-%d'))
				self.mkdir_if_not_exist(arch_path)
				sfiles = sfiles + list(filter(lambda f: f.endswith('.parquet'), [path + '/{}'.format(f) for f in os.listdir(path)]))
				afiles = afiles + list(filter(lambda f: f.endswith('.parquet'), [arch_path + '/{}'.format(f) for f in os.listdir(path)]))
				date = date + day_delta
			count = len(sfiles)
			i = 0
			print('{}/{}'.format(i,count),end='')
			while i < count:
				shutil.move(sfiles[i],afiles[i])
				i += 1
				print('\r{}/{}'.format(i,count),end='')
			print(' done')
			date = dates.get('start')
			while date <= dates.get('end'):
				path = path_tmp.format(self.land, self.table, date.strftime('%Y-%m-%d'))
				self.rmdir_if_exist(path)
				date = date + day_delta
			self.log('done')
		else:
			self.log(jobhandle, 'E')
		delta = datetime.datetime.now() - timestamp
		self.log('Delta Export for {} {} done in {}'.format(
			self.land, 
			self.table,
			delta))