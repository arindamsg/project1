with pg as (
	select top (@CHUNK) "BUKRS", "BELNR", "GJAHR"
		from "DS_FSCKPI_STG"."dbo"."PMDI_BKPF"
		where
			GJAHR >= 2017
			and (
				(BUKRS = '@BUKRS' and BELNR = '@BELNR' and GJAHR > @GJAHR)
				or (BUKRS = '@BUKRS' and BELNR > '@BELNR')
				or (BUKRS > '@BUKRS')
			)
		order by "BUKRS", "BELNR", "GJAHR"
	)
	select tab."MANDT", tab."BUKRS", tab."BELNR", tab."GJAHR", tab."BLART", tab."BLDAT", tab."BUDAT", tab."MONAT", tab."CPUDT", tab."CPUTM", tab."AEDAT", tab."UPDDT", tab."WWERT", tab."USNAM", tab."TCODE", tab."BVORG", tab."XBLNR", tab."DBBLG", tab."STBLG", tab."STJAH", tab."BKTXT", tab."WAERS", tab."KURSF", tab."KZWRS", tab."KZKRS", tab."BSTAT", tab."XNETB", tab."FRATH", tab."XRUEB", tab."GLVOR", tab."GRPID", tab."DOKID", tab."ARCID", tab."IBLAR", tab."AWTYP", tab."AWKEY", tab."FIKRS", tab."HWAER", tab."HWAE2", tab."HWAE3", tab."KURS2", tab."KURS3", tab."BASW2", tab."BASW3", tab."UMRD2", tab."UMRD3", tab."XSTOV", tab."STODT", tab."XMWST", tab."CURT2", tab."CURT3", tab."KUTY2", tab."KUTY3", tab."XSNET", tab."AUSBK", tab."XUSVR", tab."DUEFL", tab."AWSYS", tab."TXKRS", tab."CTXKRS", tab."LOTKZ", tab."XWVOF", tab."STGRD", tab."PPNAM", tab."PPDAT", tab."PPTME", tab."BRNCH", tab."NUMPG", tab."ADISC", tab."XREF1_HD", tab."XREF2_HD", tab."XREVERSAL", tab."REINDAT", tab."RLDNR", tab."LDGRP", tab."PROPMANO", tab."XBLNR_ALT", tab."VATDATE", tab."DOCCAT", tab."XSPLIT", tab."CASH_ALLOC", tab."FOLLOW_ON", tab."XREORG", tab."SUBSET", tab."KURST", tab."KURSX", tab."KUR2X", tab."KUR3X", tab."XMCA", tab."RESUBMISSION", tab."/SAPF15/STATUS", tab."PSOTY", tab."PSOAK", tab."PSOKS", tab."PSOSG", tab."PSOFN", tab."INTFORM", tab."INTDATE", tab."PSOBT", tab."PSOZL", tab."PSODT", tab."PSOTM", tab."FM_UMART", tab."CCINS", tab."CCNUM", tab."SSBLK", tab."BATCH", tab."SNAME", tab."SAMPLED", tab."EXCLUDE_FLAG", tab."BLIND", tab."OFFSET_STATUS", tab."OFFSET_REFER_DAT", tab."PENRC", tab."KNUMV", tab."PYBASTYP", tab."PYBASNO", tab."PYBASDAT", tab."PYIBAN", tab."INWARDNO_HD", tab."INWARDDT_HD"
		from "DS_FSCKPI_STG"."dbo"."PMDI_BKPF" as tab
		join pg on
			tab.BUKRS = pg.BUKRS
			and tab.BELNR = pg.BELNR
			and tab.GJAHR = pg.GJAHR
		order by tab."BUKRS", tab."BELNR", tab."GJAHR"