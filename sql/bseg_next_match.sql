select top 1 BUKRS, BELNR, GJAHR, BUZEI
	from DS_FSCKPI_STG.dbo.pmdi_bseg
	where
		GJAHR >= 2017
		and KOART in ('D','K')
		and (
		(BUKRS = '@BUKRS' and BELNR = '@BELNR' and GJAHR = @GJAHR and BUZEI > @BUZEI)
		or (BUKRS = '@BUKRS' and BELNR > '@BELNR')
		or (BUKRS > '@BUKRS')
		)
	order by BUKRS, BELNR, GJAHR, BUZEI