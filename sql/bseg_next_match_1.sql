select top 1 BUKRS, BELNR, GJAHR, BUZEI
	from @BSEG_TAB
	where
		GJAHR >= 2017
		and KOART in ('D','K')
		and (
		(BUKRS = '@BUKRS' and BELNR = '@BELNR' and GJAHR = @GJAHR and BUZEI > @BUZEI)
		or (BUKRS = '@BUKRS' and BELNR > '@BELNR')
		or (BUKRS > '@BUKRS')
		)
	order by BUKRS, BELNR, GJAHR, BUZEI