with pg as (
	select top (@CHUNK) "MANDANT", "OBJECTCLAS", "OBJECTID", "CHANGENR"
		from DS_FSCKPI_STG.dbo.pmdi_cdhdr
		where
			OBJECTCLAS = '@OBJECTCLAS'
			and CHANGENR > '@CHANGENR'
			and UDATE >= cast('20170101' as date)
		order by "OBJECTCLAS","CHANGENR"
	)
	select tab."MANDANT", tab."OBJECTCLAS", tab."OBJECTID", tab."CHANGENR", tab."USERNAME", tab."UDATE", tab."UTIME", tab."TCODE", tab."PLANCHNGNR", tab."ACT_CHNGNO", tab."WAS_PLANND", tab."CHANGE_IND", tab."LANGU", tab."VERSION", tab."_DATAAGING"
		from DS_FSCKPI_STG.dbo.pmdi_cdhdr as tab
		join pg on
			tab.MANDANT = pg.MANDANT
			and tab.OBJECTCLAS = pg.OBJECTCLAS
			and tab.OBJECTID = pg.OBJECTID
			and tab.CHANGENR = pg.CHANGENR
		order by tab."OBJECTCLAS", tab."CHANGENR"