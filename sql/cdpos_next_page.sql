with pg as (
	select top (@CHUNK) "MANDANT", "OBJECTCLAS", "OBJECTID", "CHANGENR"
		from DS_FSCKPI_STG.dbo.pmdi_cdhdr
		where
			OBJECTCLAS = '@OBJECTCLAS'
			and CHANGENR > '@CHANGENR'
			and UDATE >= cast('20170101' as date)
		order by "OBJECTCLAS","CHANGENR"
	)
	select tab."MANDANT", tab."OBJECTCLAS", tab."OBJECTID", tab."CHANGENR", tab."TABNAME", tab."TABKEY", tab."FNAME", tab."CHNGIND", tab."TEXT_CASE", tab."UNIT_OLD", tab."UNIT_NEW", tab."CUKY_OLD", tab."CUKY_NEW", tab."VALUE_NEW", tab."VALUE_OLD", tab."_DATAAGING"
		from DS_FSCKPI_STG.dbo.pmdi_cdpos as tab
		join pg on
			tab.MANDANT = pg.MANDANT
			and tab.OBJECTCLAS = pg.OBJECTCLAS
			and tab.OBJECTID = pg.OBJECTID
			and tab.CHANGENR = pg.CHANGENR
		order by tab."OBJECTCLAS", tab."CHANGENR"
